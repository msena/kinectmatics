KinectMatlabTools v1.0
----------------------

1/25/14 - Mark Sena  
	Creating a GIT repository for managing Kinect-related code used for research in the Lotz lab.  
	This repo includes a static version of the [Kinect Matlab](http://www.mathworks.com/matlabcentral/fileexchange/30242-kinect-matlab) MEX wrapper for OpenNI-1.x by Dirk-Jan Kroon.  
	Source (.cpp) files for MEX may be modified/customized within this repo.