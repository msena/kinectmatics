#include "mex.h"
#include "math.h"
#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>

#define CHECK_RC(nRetVal, what)										\
	if (nRetVal != XN_STATUS_OK)									\
	{																\
		printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
        mexErrMsgTxt("Kinect Error"); 							    \
	}
    
//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
xn::Context g_Context;
xn::DepthGenerator g_DepthGenerator;

/* The matlab mex function */
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    double *Iout;
    XnUInt64 *MXadress;
    
    if(nrhs==0 || mxIsEmpty(prhs[0]))
    {
       printf("Open failed: Give Pointer to Kinect as input\n");
       mexErrMsgTxt("Kinect Error"); 
    }
    
    MXadress = (XnUInt64*)mxGetData(prhs[0]);
    if(MXadress[0]>0){ g_Context = ((xn::Context*) MXadress[0])[0]; }
    if(MXadress[2]>0)
	{ 
		g_DepthGenerator = ((xn::DepthGenerator*) MXadress[2])[0]; 
	}
	else
	{
		mexErrMsgTxt("No Depth Node in Kinect Context"); 
	}
    
    XnStatus nRetVal = XN_STATUS_OK;

	xn::DepthMetaData depthMD;
    
	// Process the data
	g_DepthGenerator.GetMetaData(depthMD);
        
  	XnUInt16 g_nXRes = depthMD.FullXRes();
	XnUInt16 g_nYRes = depthMD.FullYRes();
	const XnDepthPixel* pDepth = depthMD.Data();
    // Jdimsc no longer needed. Replaced:
	//	Jdimsc[0] with g_nXRes
	//	Jdimsc[1] with g_nYRes

    int npixels=g_nXRes*g_nYRes;
    int index;
    XnPoint3D *pt_proj =new XnPoint3D[npixels];
    XnPoint3D *pt_world=new XnPoint3D[npixels];
    for(int y=0; y<g_nYRes; y++)
    {
        for(int x=0; x<g_nXRes; x++)
        {
            index=x+y*g_nXRes;
            pt_proj[index].X=(XnFloat)x;
            pt_proj[index].Y=(XnFloat)y;
            pt_proj[index].Z=pDepth[x+y*g_nXRes];
        }
    }
    g_DepthGenerator.ConvertProjectiveToRealWorld ( (XnUInt32)npixels,   pt_proj, pt_world);
    
	// used to be: plhs[0] = mxCreateNumericArray(3, Jdimsc, mxDOUBLE_CLASS, mxREAL);
    // now outputting as npixels-by-3 array for faster linear indexing
	plhs[0] = mxCreateDoubleMatrix( npixels, 3, mxREAL );
	Iout = mxGetPr(plhs[0]);
    for(int y=0; y<g_nYRes; y++)
    {
        for(int x=0; x<g_nXRes; x++)
        {
            index=x+y*g_nXRes;
            Iout[index] =			pt_world[index].X;		// -X(cam) -> X(lab) (but then transposed, so do not negate)
            Iout[index+npixels] =	pt_world[index].Z;		// Z(cam) -> Y(lab)
            Iout[index+npixels*2] = pt_world[index].Y;		// Y(cam) -> Z(lab)
        }
    }
    
    delete[] pt_proj;
    delete[] pt_world;
}
