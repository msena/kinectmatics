function data = K_applyFcnPram( data, fcn_pram )
% K_applyFcnPram applies all functions/parameters fcnPrams to data
%
% data = K_applyFcnPram( data, fcnPram )
%
% data          data array to be operated on by functions
% fcn_pram      nFcns*2 cell array of (:,1) fcn handles and (:,2) 
%                parameter values (single, not arrays) to be used.
%                Parameter vals are NaN if the function is not used
%
% note: this simple function is important, as it allows a linear data
% processing pipeline to be defined in the cell array fcn_pram

% Mark Sena June 2013
%
% TODO
% be able to handle arrays of parameters instead of single-value
%  parameters

for m = 1:size(fcn_pram,1)
    fcn = fcn_pram{m,1};             % function handle to apply
    pram = fcn_pram{m,2};            % parameter value to apply  
    if ~isnan( pram )
        data = fcn( data, pram );
    end
end
end
