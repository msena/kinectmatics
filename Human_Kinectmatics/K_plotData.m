function hPlot = K_plotData( data, dataType, varargin )
%K_initPlot initializes plots for the given data and data type
%
%   hPlot = K_plotData( data, dataType, cam_RorL )
%   hPlot = K_plotData( ... color_rgbk )
%   K_plotData( data, dataType, hPlot, cam_RorL )
%
% data          m*n image/depth, 3*nPts points, m*n*3 surface array
% dataType      type of data to display
%   'color'       	 RGB image
%   'infrared'    	 intensity-scaled image, hybrid (gray/cool) colormap
%   'dInfrared',     infrared masked by invalid depth values
%   'depth'          intensity-scaled image, 2m-centered colormap
%   'points'         3D point cloud of all points passed in
%   'cPoints'        3D point cloud of points brighter than 80% of the max
%                       valid IR intensity value
%   'surf'           3D surface with invalid depth vals removed
% plotOpts      options for visualization
%   color_rgbk      'r' 'g' 'b' or 'k' color for plot
%   cam_RorL        'R'/'L' if the camera is rotated right or left
% hPlot         handle for the plot (graphics object)
%
% UPDATE BELOW...
%hPlot = K_plotData(data,dataType)  creates a plot for the given data
%   type and returns the handle to the plot (image, linseries, or surf').
%
%K_plotData( ..., color_rgbk )  uses the color specified by 'color_rgbk'
%   for 2D or 3D 'lineseries' plots (not supported for image plots)
%
%Note: image data is transposed

% Mark Sena June 2013
%
% TODO
% - allow cell arrays of data and data types as inputs, and array of plot
%       handles as output
% - benchmark 3D point and surface plot
% - resolve -X issue (perhaps negate X in .cpp file)
% - allow overlay if multiple dataTypes are provided in cells (e.g. 2D
%       point plots on top of images)
% - add 2D and 3D skeleton visualization
% - decide whether or not it's worth using transparency
%
% NOTES
% - set( gca, 'NextPlot', 'replacechildren' ) created axis problems
% - remember axis off to remove ticks and numbers
% - for left or right camera orientations, the 3D data has to be rotated
%   (either by matrix multiplication, or by negating/swapping entries)
%   Need to test whether or not this is slow. If so, perhaps camera
%   orientation could be taken care of inside the Mex function.

%% handle inputs
% determine whether we're initializing or updating the plot

nvIn = length(varargin);
if nvIn==0,     
    error('not enough inputs')
    
elseif ishandle(varargin{1})  
    % update existing plot associated with handle
    opts = {[],''};
    opts(1:nvIn) = varargin;
    [hPlot cam_RorL] = opts{:};
    
    updatePlot( data, dataType, hPlot, cam_RorL );
    if nvIn==1, warning('assuming neutral camera orientation'); end
     
else
    % create new plot with the given options
    opts = {'','k'};         	% default neutral cam, black plot 
    opts(1:nvIn) = varargin;                        
    [cam_RorL, color_rgbk] = opts{:};               
    
    hPlot = initPlot( data, dataType, cam_RorL, color_rgbk );

end



function hPlot = initPlot( data, dataType, cam_RorL, color_rgbk )
%% make plot for given data type, color, and camera orientation
switch dataType
    case 'color'       	% TODO: camera orientation not supported for color 
        hPlot = imshow(...
            permute(data,[3 2 1]) );

    case {'infrared', 'dInfrared', 'depth'}             % image plot
        hPlot = make2Dplot( data, dataType, cam_RorL );
        
    case {'points', 'surf'}
        hPlot = make3Dplot( data, dataType, cam_RorL, color_rgbk );
        
    case 'cPoints'
     	hPlot = make3Dplot( data{1}, 'points', cam_RorL, color_rgbk );
        hold on
     	hPlot(2) = make3Dplot( data{2}, dataType, cam_RorL, 'r', 5);
        hold off
        
    otherwise
        error('dataType must be either IR, depth, points, or surf')
end
drawnow


function updatePlot( data, dataType, hPlot, cam_RorL )
%% updates an existing plot with the data of type dataType
switch dataType
    case {'infrared', 'dInfrared', 'depth'} 	% infrared or depth image
        data = trans2Dimage( data, cam_RorL );
        set(hPlot, 'CData', data);

    case 'points'            	% 2D or 3D points
        data = trans3Dpoints( data, cam_RorL );
        set(hPlot,... 
            'XData', data(:,1),...
            'YData', data(:,2),...
            'ZData', data(:,3) );
    
    case 'cPoints'           	% 2D or 3D points
        data1 = trans3Dpoints( data{1}, cam_RorL );
        set(hPlot(1),... 
            'XData', data1(:,1),...
            'YData', data1(:,2),...
            'ZData', data1(:,3) );

        data2 = trans3Dpoints( data{2}, cam_RorL );
        set(hPlot(2),... 
            'XData', data2(:,1),...
            'YData', data2(:,2),...
            'ZData', data2(:,3) );

        
    case 'surf'                 % 3D surface
        data = trans3Dpoints( data, cam_RorL );
        if size(data,1)~=(640*480),
            error('data must be (640*480)-by-3 for surf plots');
        end
        set(hPlot,...
            'XData', data(:,1),...
            'YData', data(:,2),...
            'ZData', data(:,3) );  

    otherwise
        error('dataType must be infrared, depth, points, or surf')
end
drawnow


%% SUBFUNCTIONS
function hPlot = make2Dplot(  data, dataType, cam_RorL )
%% make image-type plot for data, depeding on orientation
data = trans2Dimage( data, cam_RorL );      % transform 2D image data
% make image plot
switch dataType
    case {'infrared', 'dInfrared'}         % intensity scaled, hybrid colormap
        hPlot = imagesc(data);
        colormap([gray(30);cool(30)])    	% upper half bits are colored
        axis off, axis image                % remove ticks, square pixels

    case 'depth'            % intensity-scaled, 2m-centered colormap
        hPlot = imagesc(data,[1500 2500]);  % shading between 1.5-2.5m  
        colormap jet
        axis off, axis image                % remove ticks, square pixels
end


function hPlot = make3Dplot(  data, dataType, cam_RorL, color_rgbk, mSz )
%% make lineseries-type or surf-type plot of data
% Transforming by matrix multiplication seems to be faster?
if nargin < 5
    mSz = 1;
end
dims = [640 480];
data = trans3Dpoints( data, cam_RorL );

% make lineseries or surf plot
switch dataType
    case {'points','cPoints'}           % 3D point cloud or 2D plot
        
        % TODO: frontal/sagittal/(transverse) plane
%         ha = newplot; hold(ha);
        % display widths
        xRng = 2000;        yRng = 1000;        zRng = 2000;
        xc = 0;             yc = 2000;          zc = 0;
        xMin = xc-xRng;     xMax = xc+xRng;
        yMin = yc-yRng;     yMax = yc+yRng;
        zMin = zc-zRng;     zMax = zc+zRng;
        % trans plane
%         patch([xMin;xMin;xMax;xMax],[yMin;yMax;yMax;yMin],[zz;zz;zz;zz],...
%             [0 0 1], 'FaceAlpha', 0.1);
%         front plane
%         patch([xMin;xMin;xMax;xMax],[yy;yy;yy;yy],[zMin;zMax;zMax;zMin],...
%             [0 1 0], 'FaceAlpha', 0.1);        
        % sag plane
%         patch([xx;xx;xx;xx],[yMin;yMin;yMax;yMax],[zMin;zMax;zMax;zMin],...
%             [1 0 0], 'FaceAlpha', 0.1);
%         
%      	axis equal, axis tight,
%         axis([xMin xMax yMin yMax zMin zMax]);

        
        hPlot = plot3( data(:,1), data(:,2), data(:,3), ...
            ['.',color_rgbk], ...
            'MarkerSize', mSz);
        set(gca, 'cameraposition', 1.0e+04 * [-0.0800   -2.7758    0.3725]);
         
        set(gca, 'cameraviewangle', 2)
        axis equal, %axis tight,
        axis([xMin xMax yMin yMax zMin zMax]);

    case 'surf'             % surface with invalid depth vals removed
        idxBad = (data(:,2)==0);              % Y==0 -> invalid depth
        
        % set invalid values to NaN so they don't appear in plot
        data(idxBad,:) = NaN;       % nice :)

        hPlot = surf(...
            reshape( data(:,1), dims ),...
            reshape( data(:,2), dims ),...
            reshape( data(:,3), dims ),...
            'EdgeColor','none',... 
            'FaceColor', color_rgbk, ...
            'FaceAlpha', 0.6); 
        camlight left; lighting flat, colormap gray, grid off; 
        axis equal, axis tight, view(3)
        % to overlay image: set(h,'CData', IR, 'FaceColor', 'texturemap');       
end


%% SUB-SUBFUNCTIONS
function image = trans2Dimage( image, cam_RorL )
%% transform image data according to camera view
switch cam_RorL
    case {'r','R'}          % laser beneath camera
        image = fliplr(image);
    case {'l','L'}          % laser above camera
        image = flipud(image);
    case ''
        image = image';       % standard position
    otherwise
        error('camera orientation must be "R" "L" or empty')
end


function points = trans3Dpoints( points, cam_RorL )
%% transform 3D point data
switch cam_RorL
    case {'r','R'}          % laser beneath camera
        T = [0 0 -1;  0 1 0;  1 0 0];
        points = points * T;
    case {'l','L'}          % laser above camera
        T = [0 0 1;  0 1 0;  -1 0 0];
        points = points * T;
    case ''                 
        % data = data * idendity    
    otherwise
        error('camera orientation must be "R" "L" or empty')
end