function hPlot = K_initPlot( data, dataType, ...
    color_rgbk)
%K_initPlot initializes plots for the given data and data type
%
% hPlot = K_initPlot( data, dataType )
% hPlot = K_initPlot( data, dataType, color_rgbk )
%
% data          m*n image/depth, 3*nPts points, m*n*3 surface array
% dataType      'IR', 'depth', 'points', or 'surf'
%                'IR' image plots are always scaled to the max value
%                'points' data can be 2*nPts(2D) 3*nPts(3D) or m*n*3(3D)
% color_rgbk    'r' 'g' 'b' or 'k' color for plot
% hPlot         handle for the plot (graphics object)

% Mark Sena June 2013
%
% TODO
%
% NOTES
% - set( gca, 'NextPlot', 'replacechildren' ) created axis problems
% - remember axis off to remove ticks and numbers
% - forget about transparency. wouldnt be that useful

if nargin < 3
    color_rgbk = 'k';
end

switch dataType
    case 'IR'       % intensity scaled, gamma colormap
        hPlot = imagesc(data);
        colormap([gray(30);cool(30)])       % upper half bits are colored
        axis off, axis image                % remove ticks, square pixels
        
    case 'depth'   	% intensity-scaled image
        hPlot = imagesc(data, [100 300]);   % 1 to 3 meters
        colormap jet
        axis off, axis image                % remove ticks, square pixels
        
    case 'points'   % point cloud (converted if input is a surface)
        if size(data,3) > 1, data = surf2points( data ); end
        hPlot = plot3( data(1,:), data(2,:), data(3,:), ...
            ['.',color_rgbk], ...
            'MarkerSize', 1 );
        axis equal, axis tight, view(3)
        
    case 'surf'     % surface
        hPlot = surf( data(:,:,1), data(:,:,2), data(:,:,3),...
            'EdgeColor','none',... 
            'FaceColor', color_rgbk, ...
            'FaceAlpha', 0.6); 
        camlight left; lighting flat, colormap gray, grid off; 
        axis equal, axis tight, view(3)
        % to overlay image: set(h,'CData', IR, 'FaceColor', 'texturemap');       

    otherwise
        error('dataType must be either IR, depth, points, or surf')
end
drawnow

 
% %% SUBFUNCTIONS
% function cMap = getColormap( color )
% %% get 64-bit colormap for displaying depth and image data
% N = 64;
% c = (linspace(0,1,N).^0.5)';   z = zeros(N,1);
% % 0,1 -> 1=red  (IR dots are red)
% % 1,0 -> 0=red  (closer objects are red)
% switch color
%     case 'r'
%         cMap = [c z z];
%     case 'g'
%         cMap = [z c z];
%     case 'b'
%         cMap = [z z c];
%     case 'k'
%         cMap = colormap('gray');
% end