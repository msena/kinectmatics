function hPlot = K_initPlot( data, dataType, color_rgbk, cam_RorL )
%K_initPlot initializes plots for the given data and data type
%
%   hPlot = K_initPlot( data, dataType )
%   hPlot = K_initPlot( data, dataType, color_rgbk )
%   hPlot = K_initPlot( data, dataType, color_rgbk, cam_RorL )
%
% data          m*n image/depth, 3*nPts points, m*n*3 surface array
% dataType      type of data to display
%   'color'       	 RGB image
%   'infrared'    	 intensity-scaled image, hybrid (gray/cool) colormap
%   'depth'          intensity-scaled image, 2m-centered colormap
%   'points'         3D point cloud of all points passed in
%   'surf'           3D surface with invalid depth vals removed
% color_rgbk    (opt) 'r' 'g' 'b' or 'k' color for plot
% cam_RorL      (opt) 'R'/'L' if the camera is rotated right or left
% hPlot         handle for the plot (graphics object)
%
%hPlot = K_initPlot(data,dataType)  creates a plot for the given data
%   type and returns the handle to the plot (image, linseries, or surf').
%
%K_initPlot( ..., color_rgbk )  uses the color specified by 'color_rgbk'
%   for 2D or 3D 'lineseries' plots (not supported for image plots)
%
%Note: image data is transposed

% Mark Sena June 2013
%
% TODO
% - fix broken surf plot
% - allow cell arrays of data and data types as inputs, and array of plot
%       handles as output
% - benchmark 3D point and surface plot
% - resolve -X issue (perhaps negate X in .cpp file)
% - allow overlay if multiple dataTypes are provided in cells (e.g. 2D
%       point plots on top of images)
% - add 2D and 3D skeleton visualization
% - decide whether or not it's worth using transparency
%
% NOTES
% - set( gca, 'NextPlot', 'replacechildren' ) created axis problems
% - remember axis off to remove ticks and numbers
% - for left or right camera orientations, the 3D data has to be rotated
%   (either by matrix multiplication, or by negating/swapping entries)
%   Need to test whether or not this is slow. If so, perhaps camera
%   orientation could be taken care of inside the Mex function.

%% handle inputs

if nargin < 4 || isempty(cam_RorL)
    cam_RorL = '';      % no camera rotation
end

if nargin < 3 || isempty(color_rgbk)
    color_rgbk = 'k';   % use black as linseries plot color
end

%% make plot for given data type

switch dataType
    case 'color'                	% TODO: camera orientation not supported for color 
        hPlot = imshow(...
            permute(data,[3 2 1]) );

    case {'infrared', 'depth'}             % image plot
        hPlot = make2Dplot( data, dataType, cam_RorL );
        
    case {'points', 'surf'}
        hPlot = make3Dplot( data, dataType, cam_RorL, color_rgbk );
        
    otherwise
        error('dataType must be either IR, depth, points, or surf')
end

drawnow


%% SUBFUNCTIONS
function hPlot = make2Dplot(  data, dataType, cam_RorL )
%% make image-type plot for data, depeding on orientation

% transform image data
switch cam_RorL
    case {'r','R'}          % laser beneath camera
        data = fliplr(data);                         % GUESS!!!
    case {'l','L'}          % laser above camera
        data = flipud(data);                        % OK
    case ''
        data = data';       % standard position
    otherwise
        error('camera orientation must be "R" "L" or empty')
end

% make image plot
switch dataType
    case 'infrared'         % intensity scaled, hybrid colormap
        hPlot = imagesc(data);
        colormap([gray(30);cool(30)])       % upper half bits are colored
        axis off, axis image                % remove ticks, square pixels

    case 'depth'            % intensity-scaled, 2m-centered colormap
        hPlot = imagesc(data,[1500 2500]);  % shading between 1.5-2.5m  
        colormap jet
        axis off, axis image                % remove ticks, square pixels
end


function hPlot = make3Dplot_thisWorksToo(  data, dataType, cam_RorL, color_rgbk )
%% make lineseries-type or surf-type plot of data
dims = [640 480];

% transform 3D point data       	TODO: compare speed to rotation
switch cam_RorL
    case {'r','R'}          % laser beneath camera
        X = data(:,3);
        Y = data(:,2);
        Z = -data(:,1);
    case {'l','L'}          % laser above camera
        X = -data(:,3);
        Y = data(:,2);
        Z = data(:,1);
    case ''                 
        X = data(:,1);             % TODO: why negative x for display?
        Y = data(:,2);
        Z = data(:,3);
    otherwise
        error('camera orientation must be "R" "L" or empty')
end

% make lineseries or surf plot
switch dataType
    case 'points'           % 3D point cloud or 2D plot
        hPlot = plot3( X, Y, Z, ...
            ['.',color_rgbk], ...
            'MarkerSize', 1 );
        view(3)
        axis equal, axis tight,

    case 'surf'             % surface with invalid depth vals removed
        idxBad = (Y==0);              % Y==0 -> invalid depth
        
        % set invalid values to NaN so they don't appear in plot
        X(idxBad) = NaN;
        Y(idxBad) = NaN;
        Z(idxBad) = NaN;

        hPlot = surf(...
            reshape( X, dims ),...
            reshape( Y, dims ),...
            reshape( Z, dims ),...
            'EdgeColor','none',... 
            'FaceColor', color_rgbk, ...
            'FaceAlpha', 0.6); 
        camlight left; lighting flat, colormap gray, grid off; 
        axis equal, axis tight, view(3)
        % to overlay image: set(h,'CData', IR, 'FaceColor', 'texturemap');       
end

function hPlot = make3Dplot(  data, dataType, cam_RorL, color_rgbk )
%% make lineseries-type or surf-type plot of data
% Transforming by matrix multiplication seems to be faster?
dims = [640 480];

% transform 3D point data     DOH! might not need
switch cam_RorL
    case {'r','R'}          % laser beneath camera
        T = [0 0 -1;  0 1 0;  1 0 0];
        data = data * T;
    case {'l','L'}          % laser above camera
        T = [0 0 1;  0 1 0;  -1 0 0];
        data = data * T;
    case ''                 
        % data = data * idendity    
    otherwise
        error('camera orientation must be "R" "L" or empty')
end

% make lineseries or surf plot
switch dataType
    case 'points'           % 3D point cloud or 2D plot
        hPlot = plot3( data(:,1), data(:,2), data(:,3), ...
            ['.',color_rgbk], ...
            'MarkerSize', 1 );
        view(3)
        axis equal, axis tight,

    case 'surf'             % surface with invalid depth vals removed
        idxBad = (data(:,2)==0);              % Y==0 -> invalid depth
        
        % set invalid values to NaN so they don't appear in plot
        data(idxBad,:) = NaN;       % nice :)

        hPlot = surf(...
            reshape( data(:,1), dims ),...
            reshape( data(:,2), dims ),...
            reshape( data(:,3), dims ),...
            'EdgeColor','none',... 
            'FaceColor', color_rgbk, ...
            'FaceAlpha', 0.6); 
        camlight left; lighting flat, colormap gray, grid off; 
        axis equal, axis tight, view(3)
        % to overlay image: set(h,'CData', IR, 'FaceColor', 'texturemap');       
end