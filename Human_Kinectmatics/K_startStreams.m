function [K_handles XML_PATH] = K_startStreams( varargin )
%K_startStreams start live or pre-recorded data streams
%
%   [K_handles XML_PATH] = K_startStreams
%   K_handles = K_startStreams( XML_PATH )       
%   K_handles = K_startStreams( ONI_PATH )
% 
% XML_PATH      path to XML config file containig 3D sensor settings
% ONI_PATH      file name of pre-recorded .oni file
% K_handles     array of handles to the live or pre-recorded data stream
%
%[K_handles XML_PATH] = K_startStreams  starts live data streams after
%   first asking the user for an XML config file, which is returned along
%   with handles to the data streams.
%
%K_handles = K_startStreams(XML_PATH)  starts live data streams using the 
%   device settings in the .xml config file specified by XML_PATH.
%
%K_handles = K_startStreams(ONI_PATH)  starts pre-recorded data streams for
%   the .oni file specified by ONI_PATH (which must be in the MATLAB path).
%
%Note: K_startup() is called in all cases to ensure that the MATLAB
%   environment is set up appropriately.

% Mark Sena $ June 2013 $
%
% TODO:
% - if needed, replace XML_PATH with a struct containing other setup prams
%
% NOTES:
% - For prerecorded data the XML_PATH is not used!
% - For replay, the directory is temporariliy changed to the location of 
%    of the .oni file so that it can be loaded
% - XML_PATH not findable in some cases. Switching to Program Files version
% for now

%% handle inputs
switch nargin   % num inpt args
    case 0 
%         XML_PATH = K_startup,                           % setup env. & XML
%      	K_handles = mxNiCreateContext(XML_PATH);    	% record new
XML_PATH = 'C:\Program Files (x86)\OpenNI\Data\SamplesConfig.xml';
        K_handles = mxNiCreateContext(XML_PATH);    	% record new

    case 1      % either a .xml or .oni file
        [K_handles XML_PATH] = handleTheInput( varargin{1} );

    otherwise
        error('too many inputs');
end

%% SUBFUNCTIONS
function [K_handles XML_PATH] = handleTheInput( pathIn )
%% if .xml, start live streams, if .oni start pre-recorded streams

[~, ~, fileExt] = fileparts(pathIn);  % parse input

switch fileExt
    case '.xml'         % live streams 
        if exist(pathIn,'file')
            XML_PATH = which(pathIn),
            K_startup(XML_PATH);                 	% set up env.
        else
            warning('XML file not found. Running K_startup')
            XML_PATH = which( K_startup );
        end
        
        K_handles = mxNiCreateContext(XML_PATH); 	% live

    case '.oni'     % pre-recorded streams
        XML_PATH = [];                              % not used
        if exist(pathIn,'file')
            ONI_PATH = which(pathIn),
            fullDir = fileparts(ONI_PATH);
            K_startup([]);                         	% set up env
        else
            warning('ONI file not found in path. Asking user to locate')
            [fileName fullDir] = uigetfile('.oni',...
            'SELECT .ONI FILE TO START');
            if ~fileName,   error('No .oni file selected'),  end
            ONI_PATH = [fullDir, fileName];
        end
                        
        % go to file directory, start stream, then go back
        oldDir = pwd;       
        cd(fullDir)
        K_handles = mxNiCreateContext([],ONI_PATH);
        cd(oldDir);                             

    otherwise
        error('bad extension');
end