%% WORKING SCRIPT

%% compiling .cpp files

clc
cd('C:\Users\MSena\Documents\Kinect downloads\Kinect_Matlab_version1f\Mex')

% Filename = 'mxNiDepthRealWorld_3out_wTranspOpt.cpp'
% Filename = 'mxNiDepthRealWorld_Pout.cpp'

OpenNiPathInclude = 'C:\Program Files (x86)\OpenNI\Include';
OpenNiPathLib = 'C:\Program Files (x86)\OpenNI\Lib';
mex('-v',['-L' OpenNiPathLib],'-lopenNI',['-I' OpenNiPathInclude],Filename);

%% Using new Mex functions

oniFile = 'laserUP_vert';
KinectHandles = K_startStreams(oniFile);

% old mex functiions
% D = mxNiDepth(KinectHandles);
% IR = mxNiInfrared(KinectHandles);
% XYZ = mxNiDepthRealWorld(KinectHandles);

% new ones
% [X Y Z] = mxNiDepthRealWorld_3out(KinectHandles); 
% [Xt Yt Zt] = mxNiDepthRealWorld_3out_wTranspOpt(KinectHandles,true); 
% Pt = mxNiDepthRealWorld_Pout(KinectHandles);

% abstracted MATLAB functions
% [IR,D,XYZ,P] = K_getData(KinectHandles,{'infrared','depth','surf','points'})

mxNiDeleteContext(KinectHandles);   

%% plotting

% oniFile = '\pendulum\pendulum_dispOff.oni';
% [hK XML_PATH] = K_startStreams(oniFile)

[hK XML_PATH] = K_startStreams()

t = {'infrared','depth','points','surf'};
[I D P S] = K_getData( hK, t );

figure(1),  K_plotData(I,t{1},'L','g');
figure(2),  K_plotData(D,t{2},'L','g');
figure(3),  K_plotData(P,t{3},'L','g');
figure(4),  K_plotData(S,t{4},'L','r');

mxNiDeleteContext(hK);

