function varargout = K_getData( KinectHandles, dataTypes )
%K_getData get current data from streams then update streams
%
%   data = K_getData( KinectHandles, dataTypes )
%   K_getData( KinectHandles )
%
% KinectHandles     OpenNI object that allows data access
% dataType          string/cell-array of data type/types to get
%   'color'             3-by-640-by-480 uint16, RGB image
%   'infrared'          640-by-480 uint16, grayscale image
%   'depth'             640-by-480 uint16, grayscale image (dist from cam)
%   'surf'              (640*480)-by-3 double array, 3D point coords
%   'points'           	nPts-by-3 double array, with invalid points removed
%   'skel2D'            2-by-15 int, array of image joint coords
%   'skel3D'            3-by-15 double, array of world joint coords
% varargout{end}    time stamp immediately after mxNiUpdateContext()
%
%data = K_getData(KinectHandles,dataTypes)  returns data of the type
%   specified by the cell array 'dataTypes', and updates the streams.
% For each of the different data types, the appropriate Mex function
%   from the Kinect-Matlab toolbox is called to grab a frame of data.
%
%K_getData(KinectHandles)  updates the data streams.
%
%Note: for 3D point/surface/skel data, coordinates were changed so that
%   z is vertical, x is to the right, and y is away from the camera. 

% Mark June 2013
%
% TODO
% - test skeleton and RGB photo data
% - allow for now() as alternative timestamp to hat()
% - get rid of strip of null depth values
%
% NOTES
% - uint images never contain NaNs, just 0
% - data is captured at mxNiUpdateContext(), not mxNi<data type>
% - no more calling permute()!
%   not a problem for IR/depth(INT) data, but slow for XYZ(DBL) data
%     18-34% overhead to permute XYZ. Only 1-1.5% overhead for LOG/INT
% - new mxNiDepthRealWorld_Pout function outputs npixels-by-3 array "P".
%    Also converts to more intuitive "lab" coords
%       lab (X:right Y:away Z:up) instead of cam (X:left Y:up Z:away)
% - linear/logical indexing on P is 12% faster than logical indexing on XYZ
%     idx=f{P(:,3)}; P2=P(idx,:); vs. idx=f{XYZ(::3)}; X2=X(idx);

%% handle inputs

if nargin < 1,          
    error('KinectHandles is a required input');
elseif nargin < 2                       % no data requested
    nTypes = 0;
else                                    % data requested
    if ~iscell( dataTypes )             % 1 type requested
        dataTypes = {dataTypes};
    end
    nTypes = length(dataTypes);         % multiple type requested      
end

% check requested number of outputs
if nargout < nTypes || nargout > nTypes + 1
    error( '%i or %i outputs expected, but %i requested\n',...
        nTypes, nTypes+1, nargout )
end

%% extract data and save to varargout

varargout = cell(1,nTypes+1);           % data to export

% capture data and create time stamp
mxNiUpdateContext(KinectHandles);
tStamp = hat;                           % TODO: allow now()
varargout{end} = tStamp;               

% call mxNi function for each data type requested
for k = 1:nTypes
    clear data
    
    zClip = 2800;   % distance (mm) beyond which data not shown
    irFrac = 0.7;   % 1-(fraction of red pixels shown)
    switch dataTypes{k}
        
        case 'color'        % 3-by-640-by-480 RGB color image
            data = mxNiPhoto(KinectHandles);
            
        case 'infrared'    	% 640-by-480 uint16 infrared intensity
            data = mxNiInfrared(KinectHandles);

        case 'dInfrared'    	% 640-by-480 uint16 infrared intensity
            data = mxNiInfrared(KinectHandles);
            D = mxNiDepth(KinectHandles);
            data(D>zClip | D==0) = nan;
            
        case 'depth'        % 640-by-480 uint16 distance from camera
            data = mxNiDepth(KinectHandles);
            
        case 'surf'         % (640*480)-by-3 3D points 
            % including "invalid" points where Z=0 for indexing purposes
            data = mxNiDepthRealWorld_Pout(KinectHandles);
                                                
        case 'points'       % nPts-by-3 3D points
            % gets rid of NaNs before returning data
            data = mxNiDepthRealWorld_Pout(KinectHandles);
%             idx = data(:,2)~=0;     % Y-value not eq to 0
%             data = data(idx,:);
            
    	case 'cPoints'       % nPts-by-3 3D points          % DOES THRESHOLDING
            % returns depth-clipped data{1} and intensity thresholded
            % data{2}
            temp = mxNiDepthRealWorld_Pout(KinectHandles);
            isValidDepth = temp(:,2) ~= 0 & temp(:,2) < zClip;    % distance clip
            data{1,1} = temp(isValidDepth,:);
            data{1,2} = find(isValidDepth);
            
            % get Xth %ile pixels that are not in a hole or past the
            % clipping range
            IR = mxNiInfrared(KinectHandles);
            isBright = IR > irFrac * max(IR(isValidDepth));
            
%             % dilate the IR map 4/1/14
%             eSel = ones(2);    % erode
%             dSel = strel('disk', 5);    % dilate
%             isBright = imdilate(imerode(isBright, eSel), dSel);
            
            isValidBright = isValidDepth & isBright(:);
            data{2,1} = temp(isValidBright,:);
            data{2,2} = find(isValidBright);

        case 'skel2D'       % image coordinates of skeleton joints
            temp = mxNiSkeleton(KinectHandles);  
            data = zeros(2,15);
            data(1,:) = temp(1:15,6)';
            data(2,:) = temp(1:15,7)';
            
        case 'skel3D'       % world coordinate of skeleton joints
            temp = mxNiSkeleton(KinectHandles);
            data = zeros(3,15);
            data(1,:) = -temp(1:15,3)';         % camera -x
            data(2,:) = temp(1:15,5)';          % camera z
            data(3,:) = temp(1:15,4)';          % camera y
            
    otherwise
            error('dataType %s not supported\n', dataTypes{k});
    end
    varargout{k} = data; 
end

% ************ info about skelPos **************
%   P : An array with size 225 x 7
%       The columes are
%        1 : UserID, 
%        2: Tracking-Confidence, 
%        3,4,5 : X,Y,Z in world-coordinates (mm)
%        6,7 : X,Y in image coordinates (pixels)
