function markers = K_detectMarkers( KinectHandles, prams )
%
%
%
%
% KinectHandles
% prams
% prevCentroids     previous marker centroids to improve kmeans detection
% markers           3*nMkrs array of marker points

% Mark Sena June 2013
%
% TODO
% - compare speed of region props, bwconncomp and bwlabel
% - use previous centroid locations to improve kmeans detection

% NOTES

% 2 options:
% blob detection
% get 2d position of marker centroid which may or may not have a valid depth
% get 3D positions of marker pixels (but only pixels that have valid
% depth)...this would benefit from hole filling of the depth
% map...which may be locally done now based on a window, or globally
% done as a pre-processing step to the depth map

% more flexibility:
% instead of simply using region pros

% careful w/ prams...what should it be
% will include IR fcn_prams
% might also include depth fcn_prams

%% handle inputs

if ~all( isfield( prams, {'IR', 'surf'} ) )
    error('the prams struct must have the "IR" and "surf" fields')
end

%% Blob detection by connected components
% If image processing produces distinct marker blobs (for example after
% image dilation to merge nearby pixels), then blobs can be segmented as
% connected components.
% I hypothesize that this is faster than k-means

% create mask
mask = K_applyFcnPram( IR, prams.IR );



%% Option 1 - project 2D marker centroids onto depth map
% this is what I did previously 
% requires region props or something similar (slow)




%% Option 2 - get 3D centroids of hicon pixels projected onto depth map
% what Asa is doing
% potential improvement in speed
% intensity information may help improve grouping (4-d kmeans)

%% speed improvement notes
% is linear indexing faster than logical??
% yes..linear indices are stored in matlab
% tip: if mask will be used alot, convert mask to linear index mIdx = find(mask)
% 

xyz = permute(XYZ,[3 1 2]);

%%  BWCONNCOMP  25% frame time


tic

% XYZ = K_applyFcnPram( XYZ, prams.surf );

%fprintf('%.2f%% frame time\n', toc/(1/30)*100)
%%%

% C = regionprops(mask,'Centroid');
CC = bwconncomp(mask);

%fprintf('%.2f%% frame time\n', toc/(1/30)*100)
%%%

N = CC.NumObjects;
mkr = zeros(N,3);
for i = 1:N
   
    % map to depth map
    idx = CC.PixelIdxList{i};
    
    % 3D coordinates for these values
%     Px = XYZ(idx);
%     Py = XYZ(idx + numel(mask));
%     Pz = XYZ(idx + 2*numel(mask));
    Px = xyz(1,idx);
    Py = xyz(2,idx);
    Pz = xyz(3,idx); 


    mkr(i,:) = [nanmean(Px), nanmean(Py), nanmean(Pz)];
    
end

fprintf('%.2f%% frame time\n', toc/(1/30)*100)

%% region props  40% frame time

tic

C = regionprops(mask,'Centroid');
N = length(C);
mkr = zeros(3,N);
for i = 1:N
    ij = round(C(i).Centroid);
    mkr(:,i) = [ 
        XYZ(ij(1),ij(2),1)
        XYZ(ij(1),ij(2),2)
        XYZ(ij(1),ij(2),3)
        ];
end

fprintf('%.2f%% frame time\n', toc/(1/30)*100)

%% BW LABEL  70-100% frame time


tic

L = bwlabel(mask);

%fprintf('%.2f%% frame time\n', toc/(1/30)*100)
%%%

N = max(L(:));
mkr = zeros(N,3);
for i = 1:N
   
    % map to depth map
    idx = L==i;
        
    % 3D coordinates for these values
    Px = xyz(1,idx);
    Py = xyz(2,idx);
    Pz = xyz(3,idx);
    
    mkr(i,:) = [nanmean(Px), nanmean(Py), nanmean(Pz)];
    
end

fprintf('%.2f%% frame time\n', toc/(1/30)*100)


%%

% extract vals from depthmap

P = XYZmaskPts( XYZ, mask );

% bah! kmeans not working
%

SZ = 1;
for i = 1:20
[label centr] = litekmeans(P,11);
sz = size(centr,2),
if sz > SZ
    sLabel = label;
    sCentr = centr;
    SZ = size(sCentr,2);
end
end




