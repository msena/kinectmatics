function [KinectHandles, fig] = K_dispStreams( oniFile, cam_RorL )
%K_dispStreams displays live or pre-recorded data streams in real time
% The user can cycle between different data types using the arrow keys
% The Kinect process continues only if KinectHandles is requested
%
% [KinectHandles, hFig] = K_dispStreams
% [KinectHandles, hFig] = K_dispStreams(oniFile)
%
% oniFile       (optional) name of pre-recorded .oni file
% KinectHandles (optional) handles to streaming Kinect data.
%                if not requested the Kinect process is stopped
% fig           (optional) handle that displays figure

% Mark Sena June 2013
%
% TODO
% - add plot overlay capability if event.Modifier==ctrl
% - fix issue where pointCloud plot loses window priority
% - perhasp add K_applyFcnPrams option??
% - other display features like pause ffd rew
%    (or have separate K_playback function for post-analysis)
%
% NOTES
% - set('windowstyle','modal') disables rotate controls
% - maintaining title inside callback is difficult


%% handle inputs

if nargin < 2
    cam_RorL = '';                              
end
if nargin < 1 || isempty(oniFile)
    KinectHandles = K_startStreams;             % live session
else
    KinectHandles = K_startStreams(oniFile);    % prerecorded session
end

%% initialize variables and figure

% variables
% dataTypes = {'infrared', 'depth', 'points'}; %, 'surf'};
% dataTypes = {'infrared', 'depth', 'points', 'cPoints'};
% dataTypes = {'infrared', 'dInfrared', 'depth', 'points', 'cPoints'};
dataTypes = {'points'};
nTypes = length(dataTypes);
idx = 1;                                        % index into dataTypes     
isDone = false;                                 % stops loop when true

% figure with callback function
fig = figure;                                   % new figure for this
set(fig,'KeyPressFcn', @changeDataType);        
hPlot = K_plotData(...
    K_getData( KinectHandles, dataTypes{idx} ),...
    dataTypes{idx},cam_RorL );
title( myTitle( dataTypes, idx ) );

%% execute plot update loop, then either stop process, continue, or record

% update plot and print FPS until return, escape or plot closed
tPrev = hat;
count = 0;
tic
while ~isDone && ishandle(hPlot(1))
    [data tNow] = K_getData( KinectHandles, dataTypes{idx} );
    K_plotData( data, dataTypes{idx}, hPlot, cam_RorL );
    fprintf( '%.1f fps\n', 1/(tNow - tPrev) );
    tPrev = tNow;
    count = count + 1;
end
disp(['avg fps = ', num2str(count/toc)]);

% STOP Kinect if KinectHandles is not requested as an output
if nargout == 0
    mxNiDeleteContext(KinectHandles);
    disp('STOPPED KINECT')
else
    disp('KINECT RUNNING')
end
if ishandle(fig)
    set(fig,'KeyPressFcn', '');                 % stop callback
    title('');
end

%% nested callback function

function changeDataType(~,event)
    switch event.Key
        case 'leftarrow'
            idx = max(1,idx-1);
        case 'rightarrow'
            idx = min(nTypes,idx+1);
        case {'return', 'escape'}
            isDone = true;
        otherwise
            disp('not a valid key')
    end
    hPlot = K_plotData(...                      % new plot to update
        K_getData( KinectHandles, dataTypes{idx} ), ...
        dataTypes{idx},cam_RorL );            
    title( myTitle( dataTypes, idx ) );         % update title
end

end

%% SUBFUNCTIONS

function myTitle = myTitle( dataTypes, idx )
%% generates title given the selected dataType
typeList = cellfun( @(s) [s,'  '], dataTypes, ...
    'UniformOutput', false );
thisType = ['[', dataTypes{idx},'] '];
typeList{idx} = thisType;

myTitle = {'\bf PREVIEW (not recording)\rm', ...
    ['LEFT / RIGHT  ( ', [typeList{:}], ')'],...
'ENTER / ESC / close  (done)'};

end


