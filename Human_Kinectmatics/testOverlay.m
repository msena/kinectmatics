KinectHandles = K_startStreams;             % live session

pts = K_getData( KinectHandles, 'points' );
IR = K_getData( KinectHandles, 'infrared' );

thresh = 0.8

idx = find(IR > thresh);
pts2 = pts(idx,:);          % bright points


hPlot1 = plot3( pts(:,1), pts(:,2), pts(:,3), ...
            ['.','k'], ...
            'MarkerSize', 1);
 
hPlot2 = plot3( pts2(:,1), pts2(:,2), pts2(:,3), ...
            ['.','r'], ...
            'MarkerSize', 2);

% loop

while ishandle(hPlot1)
    
    pts = K_getData( KinectHandles, 'points' );
    IR = K_getData( KinectHandles, 'infrared' );
    
    idx = find(IR > thresh);
    pts2 = pts(idx,:);          % bright points

    
    set(hPlot1,...
        'XData', pts(:,1),...
        'YData', pts(:,2),...
        'ZData', pts(:,3) );
    
    
    set(hPlot1,...
        'XData', pts(:,1),...
        'YData', pts(:,2),...
        'ZData', pts(:,3) );
    
end