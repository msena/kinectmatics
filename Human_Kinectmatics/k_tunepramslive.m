function FCN_PRAM_OUT = K_tunePramsLive( ...
    KinectHandles, dataInType, fcn_prams, ...
    dataOutType, cam_RorL)
% K_tunePramsLive interactively tune parameters on live Kinect data
%
% FCN_PRAM_OUT = K_tunePramsLive(KinectHandles, dataInType, fcn_prams )
% FCN_PRAM_OUT = K_tunePramsLive(KinectHandles, dataInType, fcn_prams, dataOutType )
%
% Takes the data stream associated with KinectHandles and sequentially
% applies the functions/parameters contained in cell array 'fcn_prams'.
% On the displayed figure, the user can cycle through functions and
% parameter values using the ARROW KEYS. BACKSPACE to revert, ESCAPE to
% reset, and ENTER to return the final parameter values
%
% KinectHandles     OpenNI object used to access Kinect data
% dataInType    input data type (IR, depth, points, or surf)
% fcn_prams     nFcns*2 cell array of (:,1) fcn handles and (:,2)
%                arrays of parameter values (ith array is 1*nPrams(i))
% dataOutType   (optional) output data type. This specifies the 
%                type of plot needed to display the output
% FCN_PRAMS_OUT nFcns*2 cell array of (:,1) fcn handles and (:,2) 
%                parameter values (single, not arrays) to be outputted.
%                Parameter vals are NaN if the function is not used
% note: CAPS variables are global in this workspace due to assignin()

% Mark Sena June 2013
%
% TODO
% -rewrite like K_liveDisplay (no need for big global variables?)
% -provide KinectHandles as an output. Delete them if not requested

%% handle inputs
if nargin < 5
    cam_RorL = '';
end
if nargin < 4                   % assume functions do not change data type
    dataOutType = dataInType;
end

nFcns = size(fcn_prams,1);
nPrams = cellfun( @length, fcn_prams(:,2) );

%% initialize parameters, data, and figures

% "global" variables written to the workspace by assignin()
FCN_IDX = 1;
% P_IDX = ones(nFcns,1);
P_IDX = zeros(nFcns,1);
FCN_PRAM_OUT = getPrams(fcn_prams, P_IDX); % parameters to be tuned
IS_DONE_TUNING = false;                 % whether or not to continue looping

% grab first frame of data and apply functions to it
data = K_getData( KinectHandles, dataInType );
data = K_applyFcnPram( data, FCN_PRAM_OUT );

% initialize figure, plot, title
hFig = figure;      figure(hFig)
hPlot = K_plotData( data, dataOutType, cam_RorL );
hTitle = title( get(hFig, 'CurrentAxes'), '');
set(hTitle, 'interpreter', 'none' )
updateTitle( hTitle, FCN_PRAM_OUT, FCN_IDX )


%% update data in loop, applying and updating prams
while ~IS_DONE_TUNING
    UPDATE_PRAMS( FCN_PRAM_OUT, FCN_IDX, P_IDX,...
        fcn_prams, nFcns, nPrams, ...
        KinectHandles, dataInType,...
        hFig, hPlot, hTitle, dataOutType, cam_RorL)
    pause(0.1)      % 10 fps display
end

% remove unused function calls from fcn_prams
FCN_PRAM_OUT( isnan( [FCN_PRAM_OUT{:,2}] ), : ) = [];

% stop callback now that user is done (can return value now)
set(hFig, 'KeyPressFcn','');  
close(hFig)


end % main


%% SUBFUNCTIONS
% -------------
%% user updates parameters, which are written to the 'caller' workspace
function UPDATE_PRAMS( FCN_PRAM_OUT, FCN_IDX, P_IDX,...
        fcn_prams, nFcns, nPrams, ...
        KinectHandles, dataInType,...
        hFig, hPlot, hTitle, dataOutType, cam_RorL)
        
% update data, apply functions
tic     % time to update data and apply parameters
data = K_getData( KinectHandles, dataInType );
data = K_applyFcnPram( data, FCN_PRAM_OUT );
disp(['fps: ', num2str(1/toc,2)])

% update plot of the result
K_plotData( data, dataOutType, hPlot, cam_RorL );
updateTitle( hTitle, FCN_PRAM_OUT, FCN_IDX )

% set callback function for figure
set(hFig,...
    'NumberTitle', 'off', ...
    'Name','ArrowKey-interactive figure',...
    'KeyPressFcn',@myCallback)

    %% nested function
    function myCallback(~,event)
        key = event.Key;
        % indices into parameter array
        [FCN_IDX, P_IDX, IS_DONE_TUNING] = updateIndices(...
            FCN_IDX, P_IDX, nFcns, nPrams, key );
        
        % corresponding parameters
        switch key
            case 'backspace'       % reset view
                FCN_PRAM_OUT = getPrams( fcn_prams, zeros(nFcns,1) );
            case 'escape'    % reset prams  
                P_IDX = zeros(nFcns,1);
                FCN_PRAM_OUT = getPrams( fcn_prams, P_IDX );
            otherwise           % update prams
                FCN_PRAM_OUT = getPrams( fcn_prams, P_IDX );
        end
        
        % assign parameters and indices to main function
        assignin('caller', 'FCN_PRAM_OUT', FCN_PRAM_OUT);
        assignin('caller', 'FCN_IDX', FCN_IDX);
        assignin('caller', 'P_IDX', P_IDX);
        assignin('caller', 'IS_DONE_TUNING', IS_DONE_TUNING);
        assignin('base', 'dataOut', data);
    end
    %%

end


%% gets parameters from the list given indices
function fcn_prams_out = getPrams(fcn_prams, pIdx)
fcn_prams_out = fcn_prams;
for m = 1:length(fcn_prams)
    prams = fcn_prams{m,2};
    if pIdx(m) > 0
        fcn_prams_out{m,2} = prams(pIdx(m));
    else
        fcn_prams_out{m,2} = NaN;
    end
end
end


%% update function index m and parameter indices n_m
function [m, n_m, isDone] = updateIndices(m, n_m, M, N_m, key)
isDone = false;
switch key
    case 'leftarrow'
        m = m-1;                % previous function
    case 'rightarrow'
        m = m+1;                % next function
    case 'downarrow'
        n_m(m) = n_m(m)-1;  	% previous param
    case 'uparrow'
        n_m(m) = n_m(m)+1;      % next param val
    case 'return'
        isDone = true;          % all done
    otherwise   % do nothing
end
% check for indices in range
if m<1,          	m=1;                end
if m>M,          	m=M;                end
if n_m(m)<1,        n_m(m)=0;       	end
if n_m(m)>N_m(m),   n_m(m)=N_m(m);      end
end


%% updates figure title to show current function and parameters
function updateTitle( hTitle, fcn_pram, fcnIdx )
fcn = fcn_pram{fcnIdx,1};        % current function in use
prams = [fcn_pram{:,2}];            % num array of parameter values
set( hTitle, 'string', ...
    { 'LEFT/RIGHT (function)  |  UP/DOWN (param)  |  BACKSPACE (revert)  |  ENTER (done)', ...
    [ 'current function:  ',    func2str(fcn) ],...
    [ 'function parameters:  ',  num2str(prams, 3) ]...
    });
end

