- detection
  kMeans vs blob detection

- remove laser dots


- speed checks
   comparing display and non-display timestamps
 x other speed optimizations (whoa...went pretty far down this road)

- excel sheet auto import and record
  eventually...
  also do simple button interface

- time-stamps

  pendulum test executed...need tracking code before I can quantify performance
  synchronization between computers. Ask Julia

- vertical view
  add option in K_initPlot
   
- marker & volume calibration
  test for distortion?
  wall - marker calibration
  2-camera averaging?



- data merging
  2-camera data merge 
  time-block data merge (achieve min samples per block)

- marker/body/chain tracking



Later...
--------



- note defaults in documentation



- update tunePrams to work similarty to display
  It's broken now! 

- guards around 'hat' timer function for computers that don't have it
  should probably have this compiled somewhere other than the DB folder

- profiler

- remove / change surf2points


Misc notes
--------
- permuting data dimensions takes time
- linear and logical indexing are faster for manipulating data
- bsxfun instead of repmatting for offsetting matrices by singleton vectors

- better to have points as nPts-by-3 array (natural indexing)
- can write surface data directly to this format inside mex function
  (instead of creating an Nx-by-Ny-by-3 array or 3 Nx-by-Ny arrays)

- what's recognized by organizer thing? TODO and NOTE? or NOTES?

