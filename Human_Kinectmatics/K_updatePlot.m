function K_updatePlot( data, dataType, hPlot)
%K_updatePlot update plots for the given data and data type
%
% K_updatePlot( data, dataType, hPlot )


% Mark Sena June 2013
%
% TODO
% - test 2D point plot capabilities

% swap X/Y for X/YData ??????

if ~ishandle(hPlot)
    error('plot handle no longer exists! Cant update plot')
end
    
switch dataType
    case {'infrared','depth'} 	% infrared or depth image
        set(hPlot, 'CData', data);

    case 'points'            	% 2D or 3D points
        set(hPlot,...                       % x and y data
            'XData', data(:,1),...
            'YData', data(:,2) );
        if size(data,2) == 3
            set(hPlot,...                   % z data if 3D
                'ZData', data(:,3) );
        end

    case 'surf'                 % 3D surface
        if size(data,1)~=(640*480),
            error('data must be (640*480)-by-3 for surf plots');
        end
        set(hPlot,...
            'XData', data(:,1),...
            'YData', data(:,2),...
            'ZData', data(:,3) );  

    otherwise
        error('dataType must be either IR, depth, points, or surf')
end

drawnow