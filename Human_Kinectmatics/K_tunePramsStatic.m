function fcn_pram_out = K_tunePramsStatic( data, ~, fcn_prams, dataOutType)
% K_tunePramsStatic interactively tune function parameters for plots
% 
% fcn_pram_out = K_tunePramsStatic( data, fcn_prams, dataOutType )
%
% Takes the data stream associated with KinectHandles and sequentially
% applies the functions/parameters contained in cell array 'fcn_prams'.
% On the displayed figure, the user can cycle through functions and
% parameter values using the ARROW KEYS. BACKSPACE to revert and ENTER
% to exit the program, returning the final parameter values
%
% data          array of data to be operated on
% fcn_prams     nFcns*2 cell array of (:,1) fcn handles and (:,2)
%                arrays of parameter values (ith array is 1*nPrams(i))
% dataOutType   output data type. This specifies the 
%                type of plot needed to display the output
% fcn_pram_out nFcns*2 cell array of (:,1) fcn handles and (:,2) 
%                parameter values (single, not arrays) to be outputted.
%                Parameter vals are NaN if the function is not used

% Mark Sena June 2013


%% handle inputs

fcnList = fcn_prams(:,1);
pramsList = fcn_prams(:,2);

hFig = figure;
hPlot = K_initPlot( data, dataOutType );

nFcns = length(fcnList);                    % # of functions to call
nPrams = cellfun( @length, pramsList );    % # of parameter values per cell

%% initialize indices for function list and parameter list
fcnIdx = 1;                  % function number as ordered in fcnList
pIdx = zeros(nFcns,1);        % n(m): parameter number as ordered in prams
currPrams = getPrams(pramsList, pIdx);

hTitle = title( get(hFig, 'CurrentAxes'), '');
set(hTitle,'interpreter','none')
instr = 'LEFT/RIGHT (function)  |  UP/DOWN (param)  |  BACKSPACE |  >>Y (done)';
updateTitle( hTitle, instr, fcnList{fcnIdx}, currPrams )


%% invoke callback function
set(hFig,...
    'NumberTitle', 'off', ...
    'Name','ArrowKey-interactive figure',...
    'KeyPressFcn',@myCallback)

% pause execution to wait for user to say "y" for done
disp('hit ANY KEY when done')
comWinInp = input('Done? [y/n] ', 's');
while ~strcmpi( comWinInp, 'y' )
    comWinInp = input('Done? [y/n] ', 's');
end

% remove unused function calls from fcn_prams
fcn_pram_out = [fcnList, currPrams];       
fcn_pram_out( isnan( [fcn_pram_out{:,2}] ), : ) = [];

% stop callback now that user is done (can return value now)
updateTitle( hTitle, 'DONE', fcnList{fcnIdx}, currPrams )
set(hFig, 'KeyPressFcn','');   
   

%% nested callback function
    function myCallback(~, event)

    % update parameter indices based on keystroke rules
    key = event.Key;        % pressed key
    [fcnIdx, pIdx] = keyUpdate(fcnIdx, pIdx, nFcns, nPrams, key);
    
    currPrams = getPrams(pramsList, pIdx);        % current parameters   
    
    % apply functions with current parameters or revert/reset
    switch key
        case 'backspace'       % reset view
            dataNew = data;
            currPrams = getPrams( pramsList, zeros(nFcns,1) );
        case 'escape'    % reset prams
            dataNew = data;
            pIdx = zeros(nFcns,1);
            currPrams = getPrams( pramsList, pIdx );
        otherwise           % update prams and data
            currPrams = getPrams( pramsList, pIdx );
            tic
            dataNew = K_applyFcnPram( data, [fcnList, currPrams] );
            fprintf('%.2f%% frame\n', toc/(1/30)*100) % $$$$

    end
    
    % update figure, title
    K_updatePlot( dataNew, dataOutType, hPlot );
    updateTitle( hTitle, instr, fcnList{fcnIdx}, currPrams )
            
    end % callback fcn
    
end % main


%% SUBFUNCTIONS
% ------------  
%% update function and parameter indices m and n_m
function [m n_m] = keyUpdate(m, n_m, M, N_m, key)
switch key
    case 'leftarrow'
        m = m-1;                    % previous function
    case 'rightarrow'
        m = m+1;                    % next function
    case 'downarrow'
        n_m(m) = n_m(m)-1;      % previous param
    case 'uparrow'
        n_m(m) = n_m(m)+1;      % next param val
    otherwise   % do nothing
end

% check for indices in range
if m<1,          	m=1;                end
if m>M,          	m=M;                end
if n_m(m)<1,        n_m(m)=0;       	end
if n_m(m)>N_m(m),   n_m(m)=N_m(m);      end

end


%% extract indexed parameter sets from the cell-array prams
function pramsOut = getPrams(pramsList, n_m)
pramsOut = pramsList;
for m = 1:length(pramsList)
    if n_m(m) > 0
        pramsOut{m} = pramsList{m}(n_m(m));
    else
        pramsOut{m} = NaN;
    end
end
end


%% updates figure title to show current function and parameters
function updateTitle( hTitle, heading, fcn, currPrams )
set( hTitle, 'string', ...
    { heading, ...
    [ 'current function:  ',    func2str(fcn) ],...
    [ 'function parameters:  ',  num2str( [currPrams{:}], 4) ]...
    });
end