function varargout = K_getData( KinectHandles, dataTypes )
%K_getData get current data from Kinect stream then update stream
%
% data = K_getData( KinectHandles, dataTypes )
% K_getData( KinectHandles )    update only
%
% KinectHandles     OpenNI object that allows data access
% dataType          string/cell-array of data type/types to get
%                    ('IR', 'depth', 'surf', 'skel2D' 'skel3D')
% varargout{end}    time stamp collected immediately after
%
% For each of the different data types, the appropriate Mex function
% from the Kinect-Matlab toolbox is called to grab a frame of data.
% For 3D point and surface data x/y/z coordinates are changed so that
% z is vertical, x is to the right, and y is away from the camera. Empty
% depth data (distance from camera==0) is set to NaN.

% Mark June 2013
%
% TODO
% - test 2D and 3D skeleton data
% - support vertical camera capture
% - allow for alternative timestamp (e.g. using now)
%
% NOTES
% - uint images can't contain NaNs
% - data is captured at mxNiUpdateContext(KinectHandles), not mxNi<data>


%% handle inputs

% number of data types to get
if nargin < 2                       % no data requested
    nTypes = 0;                     
elseif iscell( dataTypes )          % multiple types requested
    nTypes = length(dataTypes);
else                                % one type requested
    dataTypes = {dataTypes};        
    nTypes = 1;
end

% check requested number of outputs
if nargout < nTypes || nargout > nTypes + 1
    error( '%i or %i outputs expected, but %i requested (not including timestamp)',...
        nTypes, nTypes+1, nargout )
end

per_mm = 0.1;                       % scaling for 3D data (cm/mm = 0.1)


%% extract data and save to varargout

varargout = cell(1,nTypes+1);           % data to export

% capture data and create time stamp
mxNiUpdateContext(KinectHandles);
tStamp = hat;                           % high-accuracy time stamp
varargout{end} = tStamp;             % prevoiusly saved time stamp               

for k = 1:nTypes

    switch dataTypes{k}
        case 'IR'           % 480*640 uint16 infrared image
            data = permute(...
                mxNiInfrared(KinectHandles), [2 1]);

        case 'depth'        % 480 x 680 Z distance from camera in cm
            data = per_mm * permute(...
                mxNiDepth(KinectHandles),[2 1]);

        case 'points'       % 3*nPoints array of non-NaN point coords (cm)
            temp = per_mm * permute(...
                mxNiDepthRealWorld(KinectHandles), [2 1 3]);
            X = -temp(:,:,1);   Y = temp(:,:,3);    Z = temp(:,:,2);
            X(Y==0) = NaN;      Y(Y==0) = NaN;      Z(Y==0) = NaN;
            data = surf2points( X, Y, Z );

        case 'surf'         % 480*680*3 XYZ coordinate data for each pixel
            % +x is to the left, +y is up, +z is away from the Kinect
            temp = per_mm * permute(...
                mxNiDepthRealWorld(KinectHandles), [2 1 3]);
            X = -temp(:,:,1);   Y = temp(:,:,3);    Z = temp(:,:,2);
            X(Y==0) = NaN;      Y(Y==0) = NaN;      Z(Y==0) = NaN;
            data = cat( 3, X, Y, Z );

        case 'skel2D'       % image coordinates of skeleton joints
            temp = mxNiSkeleton(KinectHandles);  
            data = zeros(2,15);
            data(1,:) = temp(1:15,6)';
            data(2,:) = temp(1:15,7)';

        case 'skel3D'       % world coordinate of skeleton joints (cm)
            temp = per_mm * mxNiSkeleton(KinectHandles);  
            data = zeros(3,15);
            data(1,:) = -temp(1:15,3)';         % camera -x
            data(2,:) = temp(1:15,5)';          % camera z
            data(3,:) = temp(1:15,4)';          % camera y

    otherwise
            error(['dataType "',dataType, '" not supported'])
    end
   varargout{k} = data; 
end

% ************ info about skelPos **************
%   P : An array with size 225 x 7
%       The columes are
%        1 : UserID, 
%        2: Tracking-Confidence, 
%        3,4,5 : X,Y,Z in world-coordinates (mm)
%        6,7 : X,Y in image coordinates (pixels)
