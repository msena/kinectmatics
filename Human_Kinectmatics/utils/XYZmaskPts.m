function P = XYZmaskPts( XYZ, mask )
%XYZmaskPts extract 3D points from a surface at specified logical indices
%
%
%
% XYZ       m*n*3 surface representation
% mask      m*n logical indices
% P         3*nPts array of 3D point positions

% Mark Sena June 2013


[M N P] = size(XYZ);
if P~=3,    
    error('XYZ must be m*n*3')     
end

[m n] = size(mask);
if m~=M || n~=N || ~islogical(mask), 
    error('mask must be m*n and logical')
end

X = XYZ(:,:,1);
Y = XYZ(:,:,2);
Z = XYZ(:,:,3);

P = [ X(mask) Y(mask) Z(mask) ]';
