function P = surf2points( varargin )
% surf2points convert 3*(m*n) point cloud 'P' from m*n*3 surface 'xyz'
%
% P = surf2points(xyz)
% P = surf2points(x,y,z)

% Mark Sena June 2013

if nargin == 1
    xyz = varargin{1};
elseif nargin == 3
    xyz = cat(3, varargin{1}, varargin{2}, varargin{3});
else
    error('must input m*n*3 xyz or m*n x, y, and z')
end

P = reshape(...
    permute( xyz, [3 2 1] ), ...
    [3, size(xyz,1)*size(xyz,2)] );

P( :, any(isnan(P),1) ) = [];    % remove points with any NaN coordinates