function XYZ = maskXYZ( XYZ, mask )
% maskXYZ sets XYZ values to NaN at the indices where mask==0
%
% XYZ = maskXYZ( XYZ, mask )


% might be a faster way of doing this?
% might replace with XYZmask_2points if the surface is never needed

%% check inputs
[M N P] = size(XYZ);
if P~=3,    
    error('XYZ must be m*n*3')     
end

[m n] = size(mask);
if m~=M || n~=N || ~islogical(mask), 
    error('logIdx must be m*n and logical')
end

%% logical indexing to set masked region to NaN
logIdx = repmat( mask, [1 1 3] );
XYZ(logIdx) = NaN;
