%% MISC WORKING SCRIPT
%% profiling
clear all, close all, clc

N = 1;
tf = zeros(N,1);
for i = 1:N
    tic  
    % do stuff
    %--------------
    tf(i) = toc/(1/30)*100;
end

mxNiDeleteContext(KinectHandles);   

rng = ceil(0.2*N):N;
plot(tf),
title( num2str([median(tf(rng)), std(tf(rng))]) )

%% permutation results
%   permute(IR,[2 1])           1.5(0.03)%    uint, so fast
%   permute(D,[2 1])            1.5(0.02)%     also uint, so fast
%   permute(XYZ,[2 1 3])        17.9(0.2)%   double so slower
%   permute(XYZ,[3 2 1])        34.1(0.6)%   double so slower
%   permute(mask,[2 1])         0.8(0.04)%   logical fastest
%
% conclusion:  don't change dimensions of XYZ, instead change dimensions
% of logical mask

%% pre-recorded get Data results
% -  XYZ = mxNiDepthRealWorld();                        22.1 (0.2)%
% -  XYZ = mxNiDepthRealWorld();
%       X = XYZ(:,:,1); Y = ...                         32.3 (0.4)%
% -  [X Y Z] = mxNiDepthRealWorld_3out();               22.6 (0.2)%
%          X = X'; Y = ...                              36.2 (0.5)%
% -  [Xt Yt Zt] = mxNiDepthRealWorld_3out_wTranspOpt(); 40.2 (0.9)%  :( 
%                                                       25.7 (0.6)% w/o transpose
% -  XYZ = mxNiDepthRealWorld();
%       mxNiUpdateContext();                            98.1 (7.4)%  YIKES!
%
% conclusion:   3 outputs is just as fast as one...but better indexing
% performance later on. Also, doing transpose inside Mex fcn doesn't help

%% indexing
%Volume clipping further than 2 meters
% - Zmask = XYZ(:,:,3) > 2000;
%       XYZ2( repmat(Zmask,[1 1 3]) ) = NaN;    14.0 (0.3)% logical repmat
% - Zmask = Z > 2000;
%       X2(Zmask) = NaN; Y2(Zmask)=...          4.5 (0.2)%  logical idx
% - Zmask = find(Z > 2000);
%       X2(Zmask) = NaN; Y2(Zmask)=...          1.3 (0.1)%  linear idx
%
% conclusion:  linear indexing rocks!
%
%IR marker pixel extractio
% - Zmask = IR > 0.9*max(IR(:));
%       XYZ2( repmat(Zmask,[1 1 3]) ) = NaN;    7.6 (0.3)%
% -                                             12.9(0.4)%  dense(>0.1) mask
% - Zmask = find( IR > 0.9*max(IR(:)) );
%       X2(Zmask) = NaN; Y2(Zmask) =...         2.1 (0.1)%  IR mask
% -                                             7.1 (0.2)%  dense (>0.1) mask
% conclusion: X/Y/Z separate still better than XYZ. Linear indexing better
%  in both cases

%% even better linera indexing?? ( requires another Mex re-write )
%3-by indexing for point data (IR marker pixels only, here)
% - mask = IR > 0.9*max(IR(:));
%       Pmask = P(mask,:);                      2.0 (0.1)%
%                                               2.3 (0.1)% using find()
%       Pmask = PP(:,mask);                     2.0 (0.1)%
%                                               2.3 (0.1)% using find()
%                                               6.8 (0.2)% dense mask
%               ... slightly faster without find() for dense mask 




