function F = FCN_MASTER
% FCN_MASTER master list for single-input functions for image processing
% !!WARNING!!: changing the name or location of these functions will
%   make any previously saved function handles invalid
% A function can be called by F.<function>(<image>,<parameter>)
%  e.g. 'dilated_image = F.dilate_sz( image, 4 )'
%


% struct syntax
% <function name>_<int or double><parameter name>
F = struct(...
    'image', struct(...
        'dilate_INTsz',             @dilate_INTsz, ...
        'erode_INTsz',              @erode_INTsz, ...
        'gamma_DBLpow',             @gamma_DBLpow, ...
        'gamma1_DBLmax',            @gamma1_DBLmax, ...
        'gamma2_INTpow',            @gamma2_INTpow, ...
        'gauss_DBLsigma',           @gauss_DBLsigma, ...
        'median_INTsz',             @median_INTsz, ...
        'thresh_DBLfrac',           @thresh_DBLfrac,...
        'threshAdapt1_DBLsigma',    @threshAdapt1_DBLsigma,...
        'threshAdapt2_DBLfrac',     @threshAdapt2_DBLfrac ), ...
    'surf', struct(...
        'minX_DBLmin',              @minX_DBLmin,...
        'maxX_DBLmax',              @maxX_DBLmax,...
        'minY_DBLmin',              @minY_DBLmin,...
        'maxY_DBLmax',              @maxY_DBLmax,...
        'minZ_DBLmin',              @minZ_DBLmin,...
        'maxZ_DBLmax',              @maxZ_DBLmax,...
        'medianXYZ_INTsz',          @medianXYZ_INTsz,...
        'gaussXYZ_DBLsigma',        @gaussXYZ_DBLsigma )...
        );
 




%% IMAGE PROCESSING FUNCTIONS
function Im = dilate_INTsz(Im, sz)
% image dilation (can also use params.imdilate)
strucElem = strel('disk', sz, 4);       % circular SE (default N=4)
Im = imdilate(Im, strucElem);                   % apply erosion

function Im = erode_INTsz(Im, sz)
% image erosion (can also use params.imerode)
strucElem = strel('disk', sz, 4);       % circular SE (default N=4)
Im = imerode(Im, strucElem);                   % apply erosion

function Im = gamma_DBLpow(Im, gammaVal)
% gamma correction
ImS = single( Im );         % single to make faster??
mx = max(ImS(:));
Im = mx * (ImS/mx).^gammaVal;

function Im1 = gamma1_DBLmax(Im, div)
% integer gamma correction
Im1 = Im/div;
function Im2 = gamma2_INTpow(Im1, gammaVal)
% integer gamma correction
Im2 = power( Im1, ceil(gammaVal) );

function Im = gauss_DBLsigma(Im, sigma)
% lowpass gaussian filtering
H = fspecial('gaussian', 4, sigma);   	% construct filter
Im = imfilter(Im, H, 'replicate');          % apply filter

function Im = median_INTsz(Im, sz)
% lowpass median filtering (can also use params.medfilt2)
Im = medfilt2(Im, [sz sz]);                   % apply filter     

function Im = thresh_DBLfrac(Im, x)
% max threshold
Im = Im > x * max(Im(:));

function Idiff = threshAdapt1_DBLsigma(Im, sigma)
% adaptive gaussian thresholding based on local image intensity 
ImBlur = gauss_DBLsigma(Im, sigma);          % call gaussian filter fun
Idiff = Im - ImBlur;                           % subtract low-frequency info

function mask = threshAdapt2_DBLfrac( Idiff, thresh )
% adaptive gaussian thresholding based on local image intensity 
maxI = max(Idiff(:));
mask = Idiff > thresh * maxI;                 % apply thresholding



%% SURFACE PROCESSING FUNCTIONS
% max and min x/y/z values in volume
function XYZ = maxX_DBLmax( XYZ, m )
XYZ = maskXYZ( XYZ, XYZ(:,:,1) > m );
function XYZ = minX_DBLmin( XYZ, m )
XYZ = maskXYZ( XYZ, XYZ(:,:,1) < m );

function XYZ = maxY_DBLmax( XYZ, m )
XYZ = maskXYZ( XYZ, XYZ(:,:,2) > m );
function XYZ = minY_DBLmin( XYZ, m )
XYZ = maskXYZ( XYZ, XYZ(:,:,2) < m );

function XYZ = maxZ_DBLmax( XYZ, m )
XYZ = maskXYZ( XYZ, XYZ(:,:,3) > m );
function XYZ = minZ_DBLmin( XYZ, m )
XYZ = maskXYZ( XYZ, XYZ(:,:,3) < m );

function XYZ = medianXYZ_INTsz(XYZ, sz)
D = XYZ(:,:,3);
D( isnan(D) ) = -100;   % set to real values for filtering
Dfilt = medfilt2(D,[sz sz]);
D(D<0) = NaN;           % back to NaN
XYZ(:,:,3) = Dfilt;

function XYZ = gaussXYZ_DBLsigma(XYZ, sigma)
% lowpass gaussian filtering on depth component
H = fspecial('gaussian', 3, sigma);   	% construct filter
D = imfilter(XYZ(:,:,3), H, 'replicate');          % apply filter
XYZ(:,:,3) = D;



