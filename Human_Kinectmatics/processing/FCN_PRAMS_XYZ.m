function FCN_PRAMS_XYZ = FCN_PRAMS_XYZ

%% CTRL + ENTER to run this cell and load the master function list
F = FCN_MASTER;
%%

% AHH! THIS IS VERY SLOW.
% MIGHT NOT BE WORTH TRIMMING LIKE THIS

% assumes Z is vertical, X is horizontal, Y is away from camera

xzRng = fliplr(0:10:200);    % 10cm increments
FCN_PRAMS_XYZ = {
    F.surf.minX_DBLmin      -xzRng
    F.surf.maxX_DBLmax      xzRng
    F.surf.minY_DBLmin      0:10:200
    F.surf.maxY_DBLmax      fliplr(200:10:400)
    F.surf.minZ_DBLmin      -xzRng
    F.surf.maxZ_DBLmax      xzRng
    };
