function FCN_PRAMS_IR = FCN_PRAMS_IR

%% CTRL + ENTER to run this cell and load the master function list
F = FCN_MASTER;
%%

FCN_PRAMS_IR = {
    F.image.median_INTsz            2:5
    F.image.erode_INTsz             1:5        
    F.image.gamma_DBLpow            1:.1:3
%     F.image.gamma1_DBLmax           50:50:1000        % faster
%     F.image.gamma2_INTpow           1:20              % faster
    F.image.thresh_DBLfrac          0:.01:1
    F.image.dilate_INTsz            1:5
    F.image.erode_INTsz             1:5
    F.image.dilate_INTsz             1:5
    };
