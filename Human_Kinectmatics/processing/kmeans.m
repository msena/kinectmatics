function [label centr] = kmeans(X, k_or_initCentr)
% Perform k-means clustering.
%   X: d x n data matrix
%   k_or_initCentr: number of seeds or matrix of initial cluster centroids
% Written by Michael Chen (sth4nth@gmail.com).
% Modified by Mark Sena June 2013

% TODO
% add repetitions for more robust solution

%% handle inputs
n = size(X,2);
last = 0;

if numel( k_or_initCentr ) == 1        % random initialization
    k = k_or_initCentr;
    label = ceil(k*rand(1,n));      
elseif size( k_or_initCentr, 1) ~= 3     % error
    error('2nd argument must be k or a 3*k initial guess');
else                                    % initial centroid guess
    centr = k_or_initCentr;
    % assign samples to the nearest centers
    [~,label] = max(bsxfun(@minus,centr'*X,dot(centr,centr,1)'/2),[],1); 
end

%% perform vectorized kmeans

while any(label ~= last)
    [u,~,label] = unique(label);   % remove empty clusters
    k = length(u);
    E = sparse(1:n,label,1,n,k,n);  % transform label into indicator matrix
    centr = X*(E*spdiags(1./sum(E,1)',0,k,k));    % compute m of each cluster
    last = label;
    [~,label] = max(bsxfun(@minus,centr'*X,dot(centr,centr,1)'/2),[],1); % assign samples to the nearest centers
end
[~,~,label] = unique(label);