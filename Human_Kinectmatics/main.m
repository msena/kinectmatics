%% New cleaned up pipeline for Kinect processing
% Ultmately, we want to take in an oni file or a live Kinect stream,
% and produce a set of joint positions and joint angles (kinematics)
%
% To get there, we need to:
%
% * Perform Marker_detection for each frame
%
% * Assign a Model to the marker data for a static Calibration frame
%   (The Model can be based on Landmarks, Rigid_clusters, or a
%   Kinematic_chain)
%
% * Track the Model with respect to the detected markers for all frames
%
% * Re-initialize the model when tracking errors occur
% 
% * Calculate joint positions and joint angles from the model
%
% Each of these steps is dependent on either user input or pre-defined
% parameters which can be tuned to produce a 'better' result.
%
% Improvements in this pipeline are aimed at reducing user input and
% either hastening or automating the tuning process so that human
% kinematic data can be produced quickly and easily.
%
% Mark Sena June 2013

% TODO:
% -figure out where SAMPLE_XML_PATH needs to be
% -register cameras (if 2 cameras)
%
% NOTES:
% - want to make all these components work inside a GUI for ease of use



%% Start kinect
% Start data streaming and show live IR, depth, or XYZ feeds.

oniFile = 'pendulum_dispOn.oni',
% KinectHandles = K_startStreams( oniFile );    % no display

%% Display live feed

KinectHandles = K_dispStreams( oniFile );       % with display

%% Tune (or load) parameters based on raw streams
% Tune pre-processing parameters for functions that act on the raw data
% ...or load existing parameters

% make markers distinctly visible
prePrams.IR =  K_tunePramsLive( KinectHandles, 'IR', FCN_PRAMS_IR )

% select the 3D region of interest
prePrams.surf = K_tunePramsLive( KinectHandles, 'surf', FCN_PRAMS_XYZ, 'points')

% XYZ = K_applyFcnPram( K_getData( KinectHandles, 'surf' ), ...
%     prams.surf );
% figure,
% h = K_plotData( XYZ, 'points' );
% for i = 1:100
%     XYZ = K_applyFcnPram( K_getData( KinectHandles, 'surf' ), ...
%         prams.surf );
%     K_plotData( XYZ, 'points', h );
% end


%% Detect markers
% pass in current kinect handles and processing parameters

markers = K_detectMarkers( KinectHandles, prePrams );


%% (build model)

%% Initialize markers (or model)

%% Track markers (or model)

%% 

mxNiDeleteContext(KinectHandles);
