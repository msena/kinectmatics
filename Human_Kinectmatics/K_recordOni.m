function timeStamps = K_recordOni( saveFilename, nSecs, cam_RorL )
%K_recordOni records an .oni file, and saves it and its time stamps to file
% First the live stream is displayed as a preview.
% Recording begins by closing stopping display by ENTER / ESC / close
% If isDisplayOn, ANY KEY stops recording earlyrecording stops either by hitting any key or after nSecs
% secons. 
% If data is not being displayed, recordi displayingends after hitting ANY KEY or close
% ( data can be recorded with or without simultaneously displaying data ) 
%
% timeStamps = K_recordOni( saveFilename, nSecs )
%
% saveFilename      name of .oni file to write to
% nSecs             number of seconds to record (if fig not closed)
% isDisplayOn       whether or not to display the IR stream       
% timeStamps        vector of time stamps in seconds when data was recorded

% TODO
% - better transition from K_dispStreams
% - key for stopping recording when figure is off
%
% NOTES
% - couldnt use a static figure as a trigger for stopping recording

%% handle inputs

if nargin < 3
    cam_RorL = '';
end

isRecordAndDisplay = false;
if nargin < 2 || isempty(nSecs)
    nSecs = 30;
    isRecordAndDisplay = true;
end
nFrames = ceil( 30 * nSecs );          % 30 sec = 30*30 frames

% add extension if not included
if isempty( strfind( saveFilename, '.oni' ))
    saveFilename = [saveFilename, '.oni'];
end


%% Preview data and initialize figures

% Allow user to view data streams before starting recording
[KinectHandles, fig] = K_dispStreams;
if ~ishandle(fig),      
    fig = figure;                   % new figure if the old one was closed  
end

% notify that we're recording
str = sprintf(' "%s" for %.1f seconds\n(ANY KEY or CLOSE to stop)',...
    saveFilename, nSecs );
disp(['RECORDING',str]);
set( fig , 'NextPlot' , 'replacechildren' );    % retain title

% while recording, can only interact with figure
set( fig,...
'WindowStyle', 'modal',...        % lock figure
    'Position', get(0,'Screensize'),...     % maximize figure if not docked
    'KeyPressFcn', 'close' )                % any key to close

title(['\bfRECORDING\rm', str], 'Color', 'red', ...
    'HandleVisibility', 'off');                 % retain title


%% Start recording either with or without displaying IR stream

CaptureHandle = mxNiStartCapture(...    % start recording
    KinectHandles, saveFilename);

if isRecordAndDisplay   
    [timeStamps lastFrame] = recAndDisp( KinectHandles, fig, nFrames, ...
        cam_RorL );
else      
    close(fig);
    [timeStamps lastFrame] = recOnly( KinectHandles, nFrames );
end

% stop recording and close Kinect
mxNiStopCapture(CaptureHandle)          
mxNiDeleteContext(KinectHandles)

% nofify that we're done and save the time stamp
fprintf('DONE RECORDING.  Saved %i frames\n',...
    lastFrame );
save( [saveFilename(1:end-4),'_TS.txt'], 'timeStamps', ...  % save .txt file
    '-ascii', '-double' )


%% SUBFUNCTIONS
function [timeStamps frame] = recOnly( KinectHandles, nFrames )
%% record .oni file for nFrames or user-break
timeStamps = zeros(nFrames,1);   
for frame = 1:nFrames
    timeStamps(frame) = hat;            % high-accuracy timer
    mxNiUpdateContext(KinectHandles);
end


function [timeStamps frame] = recAndDisp( KinectHandles, fig, nFrames, cam_RorL )
%% display and record .oni file for nFrames or user-break
dataType = 'infrared';
hPlot = K_plotData(...              	% initialize new plot
    K_getData( KinectHandles, dataType),...
    dataType, ...
    cam_RorL);

timeStamps = zeros(nFrames,1);
for frame = 1:nFrames
    if ishandle(fig)                    % safest to check fig
        [data tStamp] = K_getData( KinectHandles, dataType );
        timeStamps(frame) = tStamp;     % save hat timestamp
        K_plotData( data, dataType, hPlot, ...
            cam_RorL);
    else
        break                           % stop early if plot closed
    end
end