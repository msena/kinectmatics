function XML_PATH = K_startup(XML_PATH)
%K_startup set up the environment for using the Human_Kinectmatics toolbox
%
%   XML_PATH = K_startup
%   K_startup( [] )
%   K_startup( XML_PATH )
%
% XML_PATH      path to XML config file containig 3D sensor settings
%
%XML_PATH = K_startup  asks the user for an XML config file (the path to
%   which is returned), adds the subdirectories of the Human_Kinectmatics 
%   toolbox, and the Kinect_Matlab toolbox directory to the MATLAB path.
%
%K_startup([])  assumes no XML file is needed (i.e. for pre-recorded data)
%
%K_startup(XML_PATH)  uses the XML config file specified by XML_PATH.
%
%Note: The Human_Kinectmatics\ folder must already be in the MATLAB path.

% Mark Sena June 2013
%   modified Feb 2014 to make more robust
%
% TODO:
% - test correct setup on different computers (environments?)
% - other important startup variables
%
% NOTES:
% - user should only have to do this once
% - would be nice to set up "environment variables" like XML_PATH so that I
%   don't have to pass things around so much

% different types of params that we'd like to set, save, and avoid passing
% around:
% - data access (e.g. file paths and XML configuration files for the device
% - data viewing (e.g. camera orientation, figure preferences, etc)
% - data processing (e.g. filtering operations on IR data)


%% Human_Kinectamtics toolbox directories
% add subdirectoreies of this folder containing K_startup.m 

thisDir = fileparts( mfilename('fullpath') );       % dir with K_startup.m
subDirs = genpath(thisDir);                         % subdirectories
addpath(subDirs);
fprintf('added to path: \n %s\n\n',subDirs);

%% get XML configuration file for sensor if not specified
% NOTE: other options include e.g.
%   SamplesConfigIR_mirrorOff.xml
%   SamplesConfigRGB_mirrorOn.xml
%   SamplesConfigRGB_mirrorOff.xml

if nargin < 1
    XML_PATH = 'Human_Kinectmatics/XMLconfigs/SamplesConfigIR_mirrorOff.xml';     
elseif ~exist(XML_PATH, 'dir')
%     error('XML_PATH provided is not on the MATLAB path');
end
disp(['XML_PATH: ', XML_PATH]);
% otherwise current XML_PATH (which may be []) is OK

%% Mex folder for Kinect-Matlab toolbox
% look for a known file in the toolbox. If not found, user must specify the
% Mex folder location

fileInToolbox = 'mxNiCreateContext.cpp';
mexDir = '';
while ~exist(fileInToolbox,'file')
    rmpath(mexDir);
    instr = ['MEX FILES NOT FOUND! ',...
        'Browse to the Mex\ folder containing: "', ...
        fileInToolbox,'"'];   
    disp(instr);
    mexDir = uigetdir([],instr);
    if ~mexDir
        error('SELECTION CANCELLED');
    else
        addpath(mexDir)
    end
end
mexDir = fileparts( which(fileInToolbox) );

% check folder contents and add to path
if ~containsAllMexFiles(mexDir) && isYes('Use folder anyway? (y/n): ')
    warning('MEX FOLDER MAY NOT HAVE ALL NEEDED FILES');
end

disp(['MEX path: ', mexDir]);

savepath
warning('saving path')

%% SUBFUNCTIONS
function isYes = isYes( prompt )
%% YesOrNo returns the response from the user or asks again
askAgain = true;
while askAgain
    switch input(prompt,'s')
        case {'y','Y',''}
            isYes = true;
            askAgain = false;
        case {'n','N'}
            isYes = false;
            askAgain = false;
        % otherwise ask again
    end
end

function isDirComplete = containsAllMexFiles( mexDir )
%% check whether the given mex directory has all the files listed below
mexFiles = {
    'mxNiChangeDepthViewPoint.'            
    'mxNiCreateContext.'                   
    'mxNiDeleteContext.'                   
    'mxNiDepth.'                           
    'mxNiDepthRealWorld.'                    
    'mxNiDepthRealWorld_Pout.'       % faster       
    'mxNiInfrared.'                        
    'mxNiPhoto.'                           
    'mxNiSkeleton.'                        
    'mxNiStartCapture.'                    
    'mxNiStopCapture.'                     
    'mxNiUpdateContext.' };

oldDir = pwd;
cd(mexDir);         % go to Mex\

% check for missing files
mexFiles = cellfun(@(c)[c, mexext], mexFiles,...
    'uniformoutput', false);
missing = ~ismember(mexFiles, ls(['*.', mexext]));
if any( missing )
    isDirComplete = false;
    warning('The following .mex file(s) are missing in\n %s',...
        mexDir );
    disp( mexFiles(missing) );
else
    isDirComplete = true;
end

cd(oldDir);         % go back